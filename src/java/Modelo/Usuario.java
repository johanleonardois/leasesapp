/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.List;

/**
 *
 * @author Johan
 */
public class Usuario {
    private String email;
    private String password;
    private String username;
    private String cc;
    private String tel;
    private String urlCc;
    private List<Casa> casas;
    private List<Apartamento> apartamento;
    private List<Habitacion> habitaciones;

    public Usuario() {
    }

    public Usuario(String email, String password, String username, String cc) {
        this.email = email;
        this.password = password;
        this.username = username;
        this.cc = cc;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getUrlCc() {
        return urlCc;
    }

    public void setUrlCc(String urlCc) {
        this.urlCc = urlCc;
    }

    public List<Casa> getCasas() {
        return casas;
    }

    public void setCasas(List<Casa> casas) {
        this.casas = casas;
    }

    public List<Apartamento> getApartamento() {
        return apartamento;
    }

    public void setApartamento(List<Apartamento> apartamento) {
        this.apartamento = apartamento;
    }

    public List<Habitacion> getHabitaciones() {
        return habitaciones;
    }

    public void setHabitaciones(List<Habitacion> habitaciones) {
        this.habitaciones = habitaciones;
    }
}
