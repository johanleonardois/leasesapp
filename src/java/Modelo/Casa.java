/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Johan
 */
public class Casa extends Inmueble {

    private Boolean patio;

    public Casa(Boolean patio, Integer numHab, Integer numPisos, Integer numBanios, Boolean garage, String direccion, String descripcion, String size, Double price, String urlPics, String ciudad, String propietario) {
        super(numHab, numPisos, numBanios, garage, direccion, descripcion, size, price, urlPics, ciudad, propietario);
        this.patio = patio;
    }

    

    

    public Boolean getPatio() {
        return patio;
    }

    public void setPatio(Boolean patio) {
        this.patio = patio;
    }

    @Override
    public String toString() {
        return "Casa{" + "numHab=" + numHab + ", numPisos=" + numPisos + ", numBanios=" + numBanios + ", garage=" + garage + ", direccion=" + direccion + ", descripcion=" + descripcion + ", size=" + size + ", price=" + price + ", urlPics=" + urlPics + "patio=" + patio + '}';
    }

    
    
}
