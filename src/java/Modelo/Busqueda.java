/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.ArrayList;
import java.util.PriorityQueue;
import ufps.util.colecciones_seed.ColaP;

/**
 *
 * @author Johan
 */
public class Busqueda {
    
    public static void main(String[] args) {
//        Casa casa = new Casa(true,3,2,2,false,"Av 16 #12-16 gaitan","3 ventanas","25m x 45m",5000.00);
//        Casa casa1 = new Casa(true,4,2,2,false,"Av 16 motilones","4 ventanas","25m",1520.00);
//        Casa casa2 = new Casa(false,1,2,5,false,"#12-16 la hermita","2 ventanas","45m",3250.23);
        ArrayList<Casa> x = new ArrayList();
//        x.add(casa);
//        x.add(casa1);
//        x.add(casa2);
        
        Busqueda b = new Busqueda();
        
        ColaP z = b.economica(x);
        while(!z.esVacia())
        {
            System.out.println(z.deColar());
        }
        System.out.println("\n");
        
        for(Casa c : b.porHabitaciones(x, 4))
        {
            System.out.println(c);
        }
        
        System.out.println("\n");
        for(Casa c : b.porBanios(x, 2))
        {
            System.out.println(c);
        }
        
        System.out.println("\n");
        for(Casa c : b.porDireccion(x, "motilones"))
        {
            System.out.println(c);
        }
        
        System.out.println("\n");
        for(Casa c : b.rangoDePrecio(x, 2000.00 , 10000.00))
        {
            System.out.println(c);
        }
        
    }
    
    public ColaP<Casa> economica(ArrayList<Casa> l)
    {
        ColaP<Casa> cola = new ColaP();
        for(Casa c : l)
        {
            String[] s = c.getPrice().toString().replace(".",",").split(",");
            String t = "-"+s[0];
            int var = Integer.parseInt(t);
            cola.enColar(c, var);
        }
        return cola;
    }
    
    public ArrayList<Casa> porHabitaciones(ArrayList<Casa> l, int n)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            if(c.getNumHab()==n)
            {
                aux.add(c);
            }
        }
        return aux;
    }
    
    public ArrayList<Casa> porBanios(ArrayList<Casa> l, int n)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            if(c.getNumBanios()==n)
            {
                aux.add(c);
            }
        }
        return aux;
    }
    
    public ArrayList<Casa> porPisos(ArrayList<Casa> l, int n)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            if(c.getNumPisos()==n)
            {
                aux.add(c);
            }
        }
        return aux;
    }
    
    public ArrayList<Casa> tieneGarage(ArrayList<Casa> l, boolean t)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            if(c.getGarage()==t)
            {
                aux.add(c);
            }
        }
        return aux;
    }
    
    public ArrayList<Casa> tienePatio(ArrayList<Casa> l, boolean t)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            if(c.getPatio()==t)
            {
                aux.add(c);
            }
        }
        return aux;
    }
    
    /*public ArrayList<Casa> misPropiedades(ArrayList<Casa> l, Usuario u)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            if(c.getPropietario()==u.getUsername())
            {
                aux.add(c);
            }
        }
        return aux;
    }*/
    
    public ArrayList<Casa> porDireccion(ArrayList<Casa> l, String dir)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            dir = dir.replace(" ","");
            if(c.getDireccion().replace(" ","").contains(dir))
            {
                aux.add(c);
            }
        }
        return aux;
    }
    
    public ArrayList<Casa> rangoDePrecio(ArrayList<Casa> l, double min, double max)
    {
        ArrayList<Casa> aux = new ArrayList();
        for(Casa c: l)
        {
            if(c.getPrice() <= max && c.getPrice() >= min)
                aux.add(c);
        }
        return aux;
    }
}
