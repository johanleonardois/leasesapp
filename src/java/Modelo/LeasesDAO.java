/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import DataBase.MysqlDbAdapter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Johan
 */
public class LeasesDAO implements MetodosDAO {

    @Override
    public Boolean crearUsuario(Usuario usuario) {
        MysqlDbAdapter base = new MysqlDbAdapter();
        Connection conexion = base.getConnection();
        try {
            PreparedStatement statement = conexion.prepareStatement("INSERT INTO Usuario(nombre,telefono,cedula,email,contrasena,url_cc) VALUES (?,?,?,?,?,?)");

            statement.setString(1, usuario.getUsername());
            statement.setString(2, usuario.getTel());
            statement.setString(3, usuario.getCc());
            statement.setString(4, usuario.getEmail());
            statement.setString(5, usuario.getPassword());
            statement.setString(6, usuario.getUrlCc());
            statement.execute();
            conexion.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Boolean agregarCasa(Casa casa, Usuario user) {
        MysqlDbAdapter sql = new MysqlDbAdapter();
        Connection conexion = sql.getConnection();

        try {
            PreparedStatement statement = conexion.prepareStatement("INSERT INTO Casa(id,numHab,numBanios,numPisos,size,garage,direccion,patio,propietario,estado,descripcion,price,urlpics,Ciudad) VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            statement.setInt(1, casa.getNumHab());
            statement.setInt(2, casa.getNumBanios());
            statement.setInt(3, casa.getNumPisos());
            statement.setString(4, casa.getSize());
            statement.setBoolean(5, casa.getGarage());
            statement.setString(6, casa.getDireccion());
            statement.setBoolean(7, casa.getPatio());
            statement.setString(8, user.getCc());
            statement.setBoolean(9, false);
            statement.setString(10, casa.getDescripcion());
            statement.setDouble(11, casa.getPrice());
            statement.setString(12, casa.getUrlPics());
            statement.setString(13, casa.getCiudad());
                    
            statement.execute();
            conexion.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Boolean agregarApartamento(Apartamento apartamento, Usuario usuario) {
        MysqlDbAdapter sql = new MysqlDbAdapter();
        Connection conexion = sql.getConnection();

        try {
            PreparedStatement statement = conexion.prepareStatement("INSERT INTO Apartamento(id,numHab,numBanios,numPisos,size,garage,direccion,Piso,propietario,estado,descripcion,price,urlpics,Ciudad) VALUES (null,?,?,?,?,?,?,?,?,?,?,?,?,?)");

            statement.setInt(1, apartamento.getNumHab());
            statement.setInt(2, apartamento.getNumBanios());
            statement.setInt(3, apartamento.getNumPisos());
            statement.setString(4, apartamento.getSize());
            statement.setBoolean(5, apartamento.getGarage());
            statement.setString(6, apartamento.getDireccion());
            statement.setInt(7, apartamento.getPiso());
            statement.setString(8, usuario.getCc());
            statement.setBoolean(9, false);
            statement.setString(10, apartamento.getDescripcion());
            statement.setDouble(11, apartamento.getPrice());
            statement.setString(12, apartamento.getUrlPics());
            statement.setString(13, apartamento.getCiudad());

            statement.execute();
            conexion.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public Boolean agregarHabitacion(Habitacion habitacion, Usuario usuario) {
        MysqlDbAdapter sql = new MysqlDbAdapter();
        Connection conexion = sql.getConnection();

        try {
            PreparedStatement statement = conexion.prepareStatement("INSERT INTO Habitacion(id,size,garage,direccion,propietario,estado,descripcion,servicios,price,urlpic,Ciudad) VALUES (null,?,?,?,?,?,?,?,?,?,?)");
            
            statement.setString(1, habitacion.getSize());
            statement.setBoolean(2, habitacion.getGarage());
            statement.setString(3, habitacion.getDireccion());
            statement.setString(4, usuario.getCc());
            statement.setBoolean(5, false);
            statement.setString(6, habitacion.getDescripcion());
            statement.setString(7, habitacion.getServicios());
            statement.setDouble(8, habitacion.getPrice());
            statement.setString(9, habitacion.getUrlPics());
            statement.setString(10, habitacion.getCiudad());

            statement.execute();
            conexion.close();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public ArrayList<Casa> buscarCasas() {
        ArrayList<Casa> casas = new ArrayList();
        MysqlDbAdapter mysql = new MysqlDbAdapter();
        Connection connection = mysql.getConnection();
        try {
            String sql="SELECT * FROM Casa";
            Statement sentencia= connection.prepareStatement(sql);
            ResultSet resultado=  sentencia.executeQuery(sql);
            while(resultado.next()){
                casas.add(new Casa(resultado.getBoolean(8),resultado.getInt(2), resultado.getInt(3),resultado.getInt(4), resultado.getBoolean(8),resultado.getString(7),resultado.getString(11),resultado.getString(7),resultado.getDouble(12),resultado.getString(13),resultado.getString(14),resultado.getString(9)));
            }
            return casas;
        } catch (Exception e) {
            e.printStackTrace();
        }return null;
    }

    @Override
    public List<Apartamento> buscarApartamentos() {
        List<Apartamento> apartamentos = new ArrayList();
        MysqlDbAdapter mysql = new MysqlDbAdapter();
        Connection connection = mysql.getConnection();
        try {
            String sql="SELECT * FROM Apartamento";
            Statement sentencia= connection.prepareStatement(sql);
            ResultSet resultado=  sentencia.executeQuery(sql);
            while(resultado.next()){
                apartamentos.add(new Apartamento(resultado.getInt(8),resultado.getInt(2), resultado.getInt(3),resultado.getInt(4),resultado.getBoolean(6),resultado.getString(7),resultado.getString(11),resultado.getString(7),resultado.getDouble(12),resultado.getString(13),resultado.getString(14),resultado.getString(9)));
            }
            return apartamentos;
        } catch (Exception e) {
            e.printStackTrace();
        }return null;
    }

    @Override
    public List<Habitacion> buscarHabitaciones() {
        List<Habitacion> habitaciones = new ArrayList<>();
        MysqlDbAdapter mysql = new MysqlDbAdapter();
        Connection connection = mysql.getConnection();
        try {
            String sql="SELECT * FROM Habitacion";
            Statement sentencia= connection.prepareStatement(sql);
            ResultSet resultado=  sentencia.executeQuery(sql);
            while(resultado.next()){
                habitaciones.add(new Habitacion(resultado.getString(8),resultado.getString(2),resultado.getBoolean(3), resultado.getString(4),resultado.getString(7),resultado.getDouble(9),resultado.getString(10), resultado.getString(11), resultado.getString(5)));
            }
            return habitaciones;
        } catch (Exception e) {
            e.printStackTrace();
        }return null;
    }
    
    public List<Usuario> buscarUsuarios() {
        List<Usuario> users = new ArrayList<>();
        MysqlDbAdapter mysql = new MysqlDbAdapter();
        Connection connection = mysql.getConnection();
        try {
            String sql="SELECT * FROM Usuario";
            Statement sentencia= connection.prepareStatement(sql);
            ResultSet resultado=  sentencia.executeQuery(sql);
            while(resultado.next()){
                users.add(new Usuario(resultado.getString(4),resultado.getString(5),resultado.getString(1),resultado.getString(3)));
            }
            return users;
        } catch (Exception e) {
            e.printStackTrace();
        }return null;
    }

}
