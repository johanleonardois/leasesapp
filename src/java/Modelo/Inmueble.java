/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Johan
 */
public class Inmueble {
    protected Integer numHab;
    protected Integer numPisos;
    protected Integer numBanios;
    protected Boolean garage;
    protected String direccion;
    protected String descripcion;
    protected String size;
    protected Double price;
    protected String urlPics;
    protected String ciudad;
    protected String propietario;

    public Inmueble(Integer numHab, Integer numPisos, Integer numBanios, Boolean garage, String direccion, String descripcion, String size, Double price, String urlPics, String ciudad, String propietario) {
        this.numHab = numHab;
        this.numPisos = numPisos;
        this.numBanios = numBanios;
        this.garage = garage;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.size = size;
        this.price = price;
        this.urlPics = urlPics;
        this.ciudad = ciudad;
        this.propietario = propietario;
    }
    
    public Integer getNumHab() {
        return numHab;
    }

    public void setNumHab(Integer numHab) {
        this.numHab = numHab;
    }

    public Integer getNumPisos() {
        return numPisos;
    }

    public void setNumPisos(Integer numPisos) {
        this.numPisos = numPisos;
    }

    public Integer getNumBanios() {
        return numBanios;
    }

    public void setNumBanios(Integer numBanios) {
        this.numBanios = numBanios;
    }

    public Boolean getGarage() {
        return garage;
    }

    public void setGarage(Boolean garage) {
        this.garage = garage;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUrlPics() {
        return urlPics;
    }

    public void setUrlPics(String urlPics) {
        this.urlPics = urlPics;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }
    
    

    @Override
    public String toString() {
        return "Inmueble{" + "numHab=" + numHab + ", numPisos=" + numPisos + ", numBanios=" + numBanios + ", garage=" + garage + ", direccion=" + direccion + ", descripcion=" + descripcion + ", size=" + size + ", price=" + price + ", urlPics=" + urlPics + '}';
    }
    
    
    
    
}
