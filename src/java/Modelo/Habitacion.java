/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Johan
 */
public class Habitacion{
    private String servicios;
    private String size;
    private Boolean garage;
    private String direccion;
    private String descripcion;
    private Double price;
    private String urlPics;
    private String ciudad;
    private String propietario;
    
    public Habitacion(){}

    public Habitacion(String servicios, String size, Boolean garage, String direccion, String descripcion, Double price, String urlPics, String ciudad, String propietario) {
        this.servicios = servicios;
        this.size = size;
        this.garage = garage;
        this.direccion = direccion;
        this.descripcion = descripcion;
        this.price = price;
        this.urlPics = urlPics;
        this.ciudad = ciudad;
        this.propietario = propietario;
    }

    

    
    
    public String getServicios() {
        return servicios;
    }

    public void setServicios(String servicios) {
        this.servicios = servicios;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public Boolean getGarage() {
        return garage;
    }

    public void setGarage(Boolean garage) {
        this.garage = garage;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUrlPics() {
        return urlPics;
    }

    public void setUrlPics(String urlPics) {
        this.urlPics = urlPics;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPropietario() {
        return propietario;
    }

    public void setPropietario(String propietario) {
        this.propietario = propietario;
    }
    
    
    

    @Override
    public String toString() {
        return "Habitacion{" + "servicios=" + servicios + ", size=" + size + ", garage=" + garage + ", direccion=" + direccion + ", descripcion=" + descripcion + '}';
    }
    
    
    
    
}
