package Modelo;

import java.util.List;


public interface MetodosDAO {
    public Boolean crearUsuario(Usuario usuario);
    public Boolean agregarCasa(Casa casa, Usuario usuario);
    public Boolean agregarApartamento(Apartamento apartamento, Usuario usuario);
    public Boolean agregarHabitacion(Habitacion habitacion, Usuario usuario);
    public List<Casa> buscarCasas();
    public List<Apartamento> buscarApartamentos();
    public List<Habitacion> buscarHabitaciones();
}
