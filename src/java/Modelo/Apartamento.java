/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Johan
 */
public class Apartamento extends Inmueble{
    private Integer piso;

    public Apartamento(Integer piso, Integer numHab, Integer numPisos, Integer numBanios, Boolean garage, String direccion, String descripcion, String size, Double price, String urlPics, String ciudad, String propietario) {
        super(numHab, numPisos, numBanios, garage, direccion, descripcion, size, price, urlPics, ciudad, propietario);
        this.piso = piso;
    }

    

    

    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    @Override
    public String toString() {
        return "Apartamento{" + "numHab=" + numHab + ", numPisos=" + numPisos + ", numBanios=" + numBanios + ", garage=" + garage + ", direccion=" + direccion + ", descripcion=" + descripcion + ", size=" + size +", piso=" + piso+ '}';
    }

    
    
    
}
