<%@page import="Modelo.Casa"%>
<%@page import="Modelo.LeasesDAO"%>
<%@ page contentType="text/html; charset=utf-8" language="java" import="java.sql.*" errorPage="" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

        <title>Busqueda</title>
        <link href="css/inicio.css" rel="stylesheet" type="text/css" />

        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

        <meta charset="utf-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                    <meta name="description"
                          content="Responsive sidebar template with sliding effect and dropdown menu based on bootstrap 3"/>
                    <title>Sidebar template</title>


                    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css"
                          integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"/>
                    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
                          integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous"/>
                    <link rel="stylesheet" href="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.min.css"/>


                    <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                        <link rel="StyleSheet" href="sidebarcss.css" type="text/css">
                            <meta charset="utf-8"/>
                            <meta name="viewport" content="width=device-width, initial-scale=1"/>
                            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
                                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

                                <link rel="stylesheet" href="CSS/main.css"/>
                                <link rel="stylesheet" href="CSS/sidebar-themes.css"/>
                                <link rel="shortcut icon" type="image/png" href="img/favicon.png" />
                                </head>

                                <body>
                                    <!--NavBar-->
                                    <div id="navbar">

                                        <img class="logo" src="IMAGENES/top.jpg" width="308" height="70" alt="topLogo" />

                                        <button class="bc1">Mis.Anuncios</button>

                                    </div>


                                    <!--SideBar-->
                                    <div class="page-wrapper default-theme sidebar-bg bg1 toggled">
                                        <nav id="sidebar" class="sidebar-wrapper">
                                            <div class="sidebar-content">
                                                <!-- sidebar-brand  -->
                                                <div class="sidebar-item sidebar-brand">
                                                    <label id="detail">DETALLES</label>
                                                    
<br/><br/><br/><br/><br/><br/>
                                                    <div id="formulario">

                                                            <div id=numericos name="numericos">

                                                                <div id="sideNums">

                                                                    <label id="numHab"  name="numHab">N.Habitaciones</label>
                                                                    <input type=number id="numHab" class="inputS" name="numHab">
                                                                </div><br/>
                                                                <div id="sideNums">
                                                                    <label id="numBath" name="numBath">N.Ba�os</label>
                                                                    <input type=number id="numBath" class="inputS" name="numBath">
                                                                </div><br/>
                                                                <div id="sideNums">
                                                                    <label id="numPiso" name="numPiso">N.Pisos</label>
                                                                    <input type=number id="numPiso" class="inputS" name="numPiso">
                                                                </div><br/>
                                                            </div>

                                                            <div id="checks">
                                                                <label>Patio</label>
                                                                <input type=checkbox id="patio" name="patio"/><br/>
                                                                <label>Garage</label>
                                                                <input type=checkbox id="garage" name="garage"/>
                                                            </div></br></br>
                                                        
                                                            <button id="btnSideBarr">Filtrar</button>

                                                    </div>



                                                </div>
                                                <!-- sidebar-header  -->
                                                <div class="sidebar-item sidebar-header d-flex flex-nowrap">

                                                </div>
                                            </div>
                                    </div>
                                    <!--Contenido-->
                                    <div class="contenido">
                                        <div class="row">

                                            <%
                                                LeasesDAO l = new LeasesDAO();
                                                for (Casa c : l.buscarCasas()){
                                            %>
                                            <div class="col-lg-4 col-md-6 mb-4">
                                                <div class="card h-100">
                                                    <a href="#"><img class="card-img-top" src="http://placehold.it/250x250" alt=""></a>
                                                    <div class="card-body">
                                                        <h4 class="card-title">
                                                            <a href="#"><%=c.getDireccion()%></a>
                                                        </h4>
                                                        <h5><%=c.getPrice()%></h5>
                                                        <p class="card-text"><%=c.getDescripcion()%></p>
                                                    </div>
                                                    <div class="card-footer">
                                                        <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <%}%>

                                        </div>
                                        <!-- /.col-lg-9 -->

                                    </div>
                                    <!-- using online scripts -->

                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"
                                            integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous">
                                    </script>
                                    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"
                                            integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous">
                                    </script>
                                    <script src="//malihu.github.io/custom-scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
                                </body>
                            </html>