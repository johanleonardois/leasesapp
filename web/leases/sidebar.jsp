<%@page import="java.util.ArrayList"%>
<%@page import="Modelo.Casa"%>
<%@page import="Modelo.LeasesDAO"%>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Busqueda</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="StyleSheet" href="sidebarcss.css" type="text/css">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

    </head>
    <body>
        <nav class="barranav">

            <img class="logo" src="https://i.imgur.com/KbwPb6Y.jpg" alt="" height="90px" width="90px"><label id="logoleases">LeasesApp</label>

        </nav>
        <div id="viewport">
            <!--SideBar-->
            <div id="sidebar">
                <header>
                    <a href="#">Detalles</a>
                </header>
                <br><br><br>
                <div class="prce">
                    <label id="price">Precio</label>
                </div>
                <ul class="nav">
                    <div class="formulario">
                        <input type="number" class="inpbox" id="precio-max" name="min">
                        MIN
                        <input type="number" class="inpbox" id="precio-min" name="max">
                        MAX         
                        <br><br><br>
                        <input type="number" class="inpbox" id="numhabitacines" name="nh">Num. Habitaciones</input>                 
                        <br><br>
                        <input type="number" class="inpbox" id="numbanios" name="nb">Num Banios</input>
                        <br>
                        <br>

                    </div>

                </ul>
                <div>
                    <input type="checkbox" class="check" id="checkG">Garage</input>
                </div>
            </div>


            <!-- Content -->

            <div class="contenido">
            
            
            <div class="row">
                
                <%
                    LeasesDAO l = new LeasesDAO();
                    for(Casa c: l.buscarCasas()){
            %>
                <div class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="#"><img class="card-img-top" src="http://placehold.it/250x250" alt=""></a>
              <div class="card-body">
                <h4 class="card-title">
                  <a href="#"><%=c.getPrice%></a>
                </h4>
                <h5><%=c.getPrice%></h5>
                <p class="card-text"><%=c.getDescripcion%></p>
              </div>
              <div class="card-footer">
                <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
              </div>
            </div>
          </div>
                        <%}%>
            <!-- /.row -->


        </div>
        <!-- /.col-lg-9 -->

    </div>
    <!-- /.row -->

</div>
</body>
</html>
